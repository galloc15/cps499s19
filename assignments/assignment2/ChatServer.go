/* Simple EchoServer in GoLang by Phu Phung, customized by Caroline Gallo for SecAD-S19*/
package main

import (
	"encoding/json"
	"fmt"
	"net"
	"os"
	"strings"
	"io/ioutil"
	"crypto/sha256"
	"encoding/hex"
	"golang.org/x/crypto/pbkdf2" 
	//"bitbucket.org/secad-s19/golang/jsondb/users"
)

const BUFFERSIZE int = 1024

type User struct {
	Username string
	Login    bool
}

type Message struct {
	Type     string
	Sender   string
	Receiver string
	Text     string
}

var listofusers = make(map[string]bool)
var allClient_conns = make(map[net.Conn]interface{})
var currentUser User
var newclient = make(chan net.Conn)
var lostclient = make(chan net.Conn)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s <port>\n", os.Args[0])
		os.Exit(0)
	}
	port := os.Args[1]
	if len(port) > 5 {
		fmt.Println("Invalid port value. Try again!")
		os.Exit(1)
	}
	server, err := net.Listen("tcp", ":"+port)
	if err != nil {
		fmt.Printf("Cannot listen on port '" + port + "'!\n")
		os.Exit(2)
	}
	fmt.Println("ChatServer in GoLang developed by Caroline Gallo, SecAD-S19")
	fmt.Printf("ChatServer is listening on port '%s' ...\n", port)
	for {
		client_conn, _ := server.Accept()
		fmt.Printf("A new client '%s' connnected!\n", client_conn.RemoteAddr().String())
		go authenticate(client_conn)
		go listen()
	} //end for

} //end main()

func listen() { //had to include this function because go func() was not working for some reason
	for {
		select {
		case client_conn := <-newclient:
			allClient_conns[client_conn] = currentUser
			if allClient_conns[client_conn] != nil {
				user := allClient_conns[client_conn].(User)
				listofusers[user.Username] = true
				go getUserList()
				go client_goroutine(client_conn)
			}
		case client_conn := <-lostclient:
			if allClient_conns[client_conn] != nil {
				user := allClient_conns[client_conn].(User)
				listofusers[user.Username] = false
				delete(listofusers, user.Username) //deletes the username from listofusers
				delete(allClient_conns, client_conn)
				message := fmt.Sprintf("A client '%s' has left!\n", user.Username)
				go sendtoAll([]byte(message))
				client_conn.Close()
			} //end if
		} //end select

	} //end for
} //end listen()

func client_goroutine(client_conn net.Conn) {
	var buffer [BUFFERSIZE]byte
	go func() {
		for {
			byte_received, read_err := client_conn.Read(buffer[0:])
			if read_err != nil {
				fmt.Println("Error in receiving...")
				lostclient <- client_conn
				return
			} //end if
			data := buffer[0:byte_received]
			go handleMessages(client_conn, data)
		} //end for
	}() //end go func()
} //end client_goroutine()

func authenticate(client_conn net.Conn) {
	var buffer [BUFFERSIZE]byte
	byte_received, read_err := client_conn.Read(buffer[0:])
	if read_err != nil {
		fmt.Println("Error in receiving...")
		lostclient <- client_conn
		return
	} //end if
	logindata := buffer[0:byte_received]
	login, username, message := login(logindata)
	if login {
		currentUser = User{Username: username, Login: true}
		newclient <- client_conn
		welcomemessage := fmt.Sprintf("A client %s has joined the chat!", username)
		go sendtoAll([]byte(welcomemessage))
		return
	} else {
		send(client_conn, []byte("Failed\n"))
		authenticate(client_conn)
	} //end else
	send(client_conn, []byte(message))
} //end authenticate()

func login(data []byte) (bool, string, string) {
	type Account struct {
		Username string
		Password string
	} //end type
	var account Account
	err := json.Unmarshal(data, &account)
	if err != nil || account.Username == "" || account.Password == "" {
		fmt.Printf("JSON parsing error: %s\n", err)
		return false, "", "Invalid login"
	} //end if
	if exist(account.Username, account.Password) {
		return true, account.Username, "You are authenticated"
	} //end if
	return false, "", "You are not authenticated"
} //end login()

func exist(username string, password string) bool {
	return myCheck(username, password)
	//return users.CheckAccount(username,password)	
	/*if username == "phu" && password == "12345" {
		return true
	} //end if
	if username == "sam" && password == "123456" {
		return true
	} //end if
	if username == "ted" && password == "password" {
		return true
	} //end if
	if username == "jenny" && password == "pass12" {
		return true
	} //end if
	if username == "debbie" && password == "wordpass" {
		return true
	} //end if*/
} //end exist()

func myCheck(username string, password string) bool {
	type Account struct {
		Username string
		Password string
	}
	type AccountDB struct {
		Users []Account
	}
	var accountDB AccountDB
	file, open_err := os.Open("users-database.json")
	defer file.Close()
	if open_err != nil {
		fmt.Println("No users-database.json file. Need to add a new user to create one.")
		json.Unmarshal([]byte(`{"users":[]}`), & accountDB)	
	} else {
		jsondatabase, read_err := ioutil.ReadAll(file)
		if read_err != nil {
			fmt.Println("Error in reading the JSON data")
			os.Exit(1)
		} //end if
		unmarshal_err := json.Unmarshal(jsondatabase, & accountDB)
		if unmarshal_err != nil {
			fmt.Println("Error in mapping the JSON data")
			os.Exit(2)
		} //end if
	}	//end else
	for i := 0; i < len(accountDB.Users); i++{
		if username == accountDB.Users[i].Username && hash(password,username) == accountDB.Users[i].Password{
			return true
		} //end if
	} //end for
 	return false
} //end myCheck()

func hash(data string, salt string) string {
	hashed_data := hex.EncodeToString(pbkdf2.Key([]byte(data), []byte(salt), 4096, 32, sha256.New))
	return hashed_data	
} //end hash()

func sendtoAll(data []byte) {
	for client_conn, _ := range allClient_conns {
		send(client_conn, data)
	} //end for
	fmt.Printf("Sent data: %s to all logged in clients\n", data)
} //end sendtoAll()

func send(client_conn net.Conn, data []byte) {
	_, write_err := client_conn.Write(data)
	if write_err != nil {
		fmt.Println("Error in sending to " + client_conn.RemoteAddr().String())
		return
	} //end if
} //end send()

func handleMessages(client_conn net.Conn, data []byte) {
	message := string(data)
	if strings.Contains(message, "private") {
		privateMessage(data)
	} else if strings.Contains(message, "Userlist") {
		sendUserList(client_conn, data)
	} else if strings.Contains(message, "public") {
		publicMessage(data)
	} //end else
} //end handleMessages()

func privateMessage(data []byte) {
	var currentMessage Message
	err := json.Unmarshal(data, &currentMessage)
	if err != nil || currentMessage.Type == "" || currentMessage.Sender == "" || currentMessage.Receiver == "" || currentMessage.Text == "" {
		fmt.Printf("JSON parsing error: %s\n", err)
	} else {
		privatemessage := fmt.Sprintf("Private message from %s: %s\n", currentMessage.Sender, currentMessage.Text)
		for client_conn, _ := range allClient_conns {
			user := allClient_conns[client_conn].(User)
			if currentMessage.Receiver == user.Username {
				go send(client_conn, []byte(privatemessage))
				fmt.Printf("Data sent: %s\n", privatemessage) //used to show the message is sent on the server side
			} //end if
		} //end for
	} //end else

} //end privateMessage()

func publicMessage(data []byte) {
	fmt.Printf(string(data))
	var publicMessage Message
	err := json.Unmarshal(data, &publicMessage)
	if err != nil || publicMessage.Text == "" || publicMessage.Type == "" || publicMessage.Sender == "" || publicMessage.Receiver == "" {
		fmt.Printf("JSON parsing error: %s\n", err)
	} else {
		publicmessagesent := fmt.Sprintf("Public message from %s: %s\n", publicMessage.Sender, publicMessage.Text)
		go sendtoAll([]byte(publicmessagesent))
	} //end else
} //end publicMessage()

func getUserList(){
	userslist := []string{}
	for user, _ := range listofusers {
		userslist = append(userslist, user) //takes the users in the listofusers and puts them in an array
	} //end for
	usersMessage := fmt.Sprintf("List of users: %s\n", userslist)
	go sendtoAll([]byte(usersMessage))
} //end getUserList()

func sendUserList(client_conn net.Conn, data []byte){
	userslist := []string{}
	for user, _ := range listofusers {
		userslist = append(userslist, user) //takes the users in the listofusers and puts them in an array
	}
	var current Message
	err := json.Unmarshal(data, &current)
	if err != nil || current.Type == "" {
		fmt.Printf("JSON parsing error: %s\n", err)
	} else {
		usermessage := fmt.Sprintf("List of users: %s\n", userslist)
		go send(client_conn, []byte(usermessage))
		fmt.Printf("Data sent: %s\n", usermessage) //used to show the message is sent on the server side

	} //end else
} //end sendUserList()
