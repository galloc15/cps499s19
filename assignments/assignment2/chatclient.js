var net = require('net');
 
if(process.argv.length != 4){
	console.log("Usage: node %s <host> <port>", process.argv[1]);
	process.exit(0);	
} //end if

var host=process.argv[2];
var port=process.argv[3];

if(host.length >253 || port.length >5 ){
	console.log("Invalid host or port. Try again!\nUsage: node %s <port>", process.argv[1]);
	process.exit(1);	
} //end if

var loggedin = false;

var client = new net.Socket();
console.log("Simple chatclient.js developed by Caroline Gallo, SecAD-S19");
console.log("Connecting to: %s:%s", host, port);
client.connect(port,host, connected);

function connected(){
	console.log("Connected to: %s:%s", client.remoteAddress, client.remotePort);
	console.log("You must enter username and password before chatting: ");
	login();
} //end connected()

var readlineSync = require('readline-sync');

var username, password;

function login(){
	username = readlineSync.question('Username:');
	password = readlineSync.question('Password:', {
		hideEchoBack: true});
	var login = JSON.stringify( {"Username": username, "Password": password} );
	client.write(login);
} //end login()


client.on("data", data=>{
	console.log("Received data: " + data);
	if (!loggedin){
		if(data != "Failed\n"){
			username = username;
			console.log("You are now logged in as " + username);
			loggedin = true;
			chat();
		}else{
			console.log("Incorrect username/password. Try again");
			login();}
	} //end if
});

client.on("error", function(err){
	console.log("Error");
	process.exit(2);
});

client.on("close", function(data){
	console.log("Connection has been disconnected");
	process.exit(3);
});

function chat(){
const keyboard = require('readline').createInterface({
	input: process.stdin,
	output: process.stdout
});

console.log("\nType anything to send to all connected clients.\nType 'To:Receiver:Message' to send a private message to a user.\nType '.userlist' to get the list of online users.\nType '.exit' to close the connection");

keyboard.on('line', (input) => {
	//console.log(`You typed: ${input}`);
	if(input === ".exit"){
		client.destroy();
		console.log("disconnected!");
		process.exit();
	}else if(input.includes("To:")){
		var string = input.split(':'); //used to get the name of the receiver and the message
		var receiver = string[1]
		var message = string[2];
		var messages = JSON.stringify( {"Type": "private", "Sender": username, "Receiver": receiver, "Text": message} );
		client.write(messages);
	}else if(input === ".userlist"){
		var usermessage = JSON.stringify( {"Type": "Userlist"} );
		client.write(usermessage);
	}else{
		var publicmessage = JSON.stringify( {"Type": "public", "Sender": username, "Receiver": "public", "Text": input} );
		client.write(publicmessage);
		} //end else
	});
} //end chat()
