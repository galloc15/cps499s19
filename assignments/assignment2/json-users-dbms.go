package main

import (
	"os"
	"fmt"
	"bitbucket.org/secad-s19/golang/jsondb/users"
	"io/ioutil"
	"encoding/json"
	"golang.org/x/crypto/pbkdf2"
	"crypto/sha256"
	"encoding/hex"
	"errors"
)

var (
	username string
	password string
)
func usage(option string) {
	fmt.Printf("Usage: %s %s\n", os.Args[0], option)
	os.Exit(0)
	
}

func MyAddAccount(username string, password string) error{
	//open file and map to json object, add new account to json object, write back to json object to file
	type Account struct {
		Username string
		Password string
	} //end type
	type AccountDB struct {
		Users []Account
	} //end type
	var accountDB AccountDB
	file, open_err := os.Open("users-database.json")
	defer file.Close()
	if open_err != nil {
		fmt.Println("No users-database.json file. Need to add a new user to create one.")
		json.Unmarshal([]byte(`{"users":[]}`), & accountDB)	
	} //end if
	jsondatabase, read_err := ioutil.ReadAll(file)
	if read_err != nil{
		return read_err
	} //end if
	json.Unmarshal(jsondatabase, & accountDB)
	accountDB.Users = append(accountDB.Users, Account{Username: username, Password: hash(password,username)})
	jsondatabase, index_err := json.MarshalIndent(accountDB, "", " ")
	if index_err != nil {
		//fmt.Printf("Error")
		return errors.New("Cannot MarshalIndent JSON object")
	} //end if
	write_err := ioutil.WriteFile("users-database.json", jsondatabase, 0644)
	if write_err != nil {
		return write_err	
	} //end if
	fmt.Println("Written successfully to users-database.json")
	return nil
} //end MyAddAccount()

func hash(data string, salt string) string {
	hashed_data := hex.EncodeToString(pbkdf2.Key([]byte(data), []byte(salt), 4096, 32, sha256.New))
	return hashed_data	
} //end hash()

func main(){
	if len(os.Args) < 2 {
		usage("--show|--check[2]|--remove|--add|--update [username] [password]")
	}
	option := os.Args[1]
	if len(os.Args) > 2 {
		username = os.Args[2]		
	}
	if len(os.Args) > 3 {
		password = os.Args[3]	
	}
	switch option{
		case "--show":
			users.ListAllAccounts()
		
		case "--remove":
			if username == "" {
				usage("--remove <username>")	
			}
			rm_err := users.RemoveAccount(username)
			if rm_err != nil {
				fmt.Printf("RemoveAccount error: %s\n", rm_err)
				return
			}
			fmt.Printf("Account '%s' is deleted\n",username)

		case "--check": 
			if username == "" || password == ""{
				usage("--check <username> <password>")	
			}
			if users.CheckAccount(username,password) {
				fmt.Printf("Testing: Account('%s','%s') is valid\n",username,password)
			} else {
				fmt.Printf("Testing: Account('%s','%s') is invalid\n",username,password)
			}	
		case "--check2": 
			if username == "" || password == ""{
				usage("--check2 <username> <password>")	
			}
			if users.CheckAccountHashed(username,password) {
				fmt.Printf("Testing: CheckAccountHashed('%s','%s') is valid\n",username,password)
			} else {
				fmt.Printf("Testing: CheckAccountHashed('%s','%s') is invalid\n",username,password)
			}	
		case "--update":
			if username == "" || password == ""{
				usage("--update <username> <password>")	
			}
			upd_err := users.UpdateAccount(username,password)
			if upd_err != nil {
				fmt.Printf("UpdateAccount error: %s\n", upd_err)
				return
			}
			fmt.Printf("Account '%s' is updated with new password\n",username)

		case "--add":
			/*if username == "" || password == ""{
				usage("--add <username> <password>")	
			}
			add_err := users.AddAccount(username,password)
			if add_err != nil {
				fmt.Printf("AddAccount error: %s\n", add_err)
				return
			}*/
			MyAddAccount(username, password)
			//fmt.Printf("Account '%s' added\n",username)
		
		default:
			fmt.Printf("%s: Unknown option\n", os.Args[0])
			usage("--show|--check[2]|--remove|--add|--update [username] [password]")
	}
}
