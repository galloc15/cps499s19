package main

import "fmt"

func main() {
    i := make(chan int)
    go func(){
	i <- 1
}()
	fmt.Println(<-i)
    //fmt.Println("hello from Caroline Gallo - SecAD-S19")
}
