import java.net.*;
import java.io.*;
import java.util.ArrayList;

public class EchoServer {
    static ThreadList threadlist = new ThreadList();
    public static void main(String[] args) throws IOException {
        
        if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        
        try {
            ServerSocket serverSocket =
                new ServerSocket(Integer.parseInt(args[0]));
            System.out.println("Caroline Gallo: EchoServer is running at port " + Integer.parseInt(args[0]));
	while(true){
            Socket clientSocket = serverSocket.accept(); 
            System.out.println("A client is connected "); 
	    //new EchoServerThread(clientSocket).start();
	    EchoServerThread newthread = new EchoServerThread(threadlist,clientSocket);
	    threadlist.addThread(newthread);
	    newthread.start();   
           }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
}

class EchoServerThread extends Thread {
	private Socket clientSocket = null;
	private ThreadList threadlist = null;
	public EchoServerThread(Socket socket){
		this.clientSocket = socket;
	}
	public EchoServerThread(ThreadList threadlist, Socket socket){
		this.clientSocket = socket;
		this.threadlist = threadlist;
}
	public void run(){
		System.out.println("A new thread for client is running");
		if(threadlist!=null){
			System.out.println("Inside thread: total clients: " + threadlist.getNumberofThreads());
}
		try{
		 PrintWriter out =
                new PrintWriter(clientSocket.getOutputStream(), true);                   
            BufferedReader in = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));
    
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                System.out.println("received from client: " + inputLine);
                System.out.println("Echo back");
                out.println(inputLine);
            }	
	} catch(IOException ioe){
		System.out.println("Exception in thread:" + ioe.getMessage());
	}
}
}

class ThreadList{
	private ArrayList<EchoServerThread> threadlist = new ArrayList<EchoServerThread>();
	public synchronized ThreadList(){
			
	}
	public synchronized int getNumberofThreads(){
		return threadlist.size();
	}
	public synchronized void addThread(EchoServerThread newthread){
		threadlist.add(newthread);
	}
	public synchronized void removeThread(EchoServerThread thread){
		threadlist.remove(thread);		
	}
	public synchronized void sendToAll(String message){
	//ask each thread in the threadlist to send the given message to its client		
	}
}
