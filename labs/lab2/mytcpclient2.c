/* include libraries */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>

int BUFFERSIZE = 1024;
char buffer[1024];

int getaddrinfo(const char *host, const char *service, const struct addrinfo *hints, struct addrinfo **result);

int main (int argc, char *argv[])
{
   printf("TCP Client program by Caroline Gallo for Lab 2 - SecAD - Spring 2019\n");
   if(argc!=3){
	printf("Usage: %s <servername> <port>\n", argv[0]);
	exit(0);
   }
   char *servername = argv[1];
   char *port = argv[2];
   if( strlen(servername) > 253 || strlen(port) > 5){
	printf("Servername or port is too long. Please try again!\n");
	exit(1);
   }
   printf("Servername= %s, port= %s\n",servername,port);
   int sockfd = socket(AF_INET, SOCK_STREAM, 0);
   if (sockfd < 0){
	perror("ERROR opening socket");
	exit(2);
   }
   printf("A socket is opened\n");
   struct addrinfo hints, *serveraddr;
   memset(&hints, 0, sizeof hints);
   hints.ai_family = AF_INET;
   hints.ai_socktype = SOCK_STREAM;
   int addr_lookup = getaddrinfo(servername, port, &hints, &serveraddr);
   if (addr_lookup != 0){
	fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(addr_lookup));
	exit(3);
   }

   int connected = connect(sockfd, serveraddr->ai_addr, serveraddr->ai_addrlen);
   if(connected < 0){
	perror("Cannot connect to the server\n");
	exit(4);
   }else
	printf("Connected to the server %s at port %s\n", servername, port);
	freeaddrinfo(serveraddr);
    char buffer[BUFFERSIZE];
    bzero(buffer, BUFFERSIZE);
    sprintf(buffer, "GET / HTTP/1.0\r\nHost: %s\r\n\r\n", servername);
    int byte_sent = send(sockfd,buffer,strlen(buffer),0);
    bzero(buffer,BUFFERSIZE);
    int byte_received = recv(sockfd, buffer, BUFFERSIZE, 0);
    if(byte_received<0){
	perror("Error in reading");
	exit(5);}
    printf("Received from server: %s", buffer);
    close(sockfd);
    return 0;
}

