<?php
	$lifetime = 15 * 60; //15 minutes
	$path = "/lab6";
	$domain = "cps499s19-galloc3.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $domain, $secure, $httponly);
	session_start();

	if (!isset($_SESSION["logged"] ) or $_SESSION["logged"] != TRUE) {
		echo"<script>alert('You have not login. Please login first');</script>";
		header("Refresh:0; url=form.php");
		die();
}
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
		echo "<script>alert('Session hijacking is detected!');</script>";
		header("Refresh:0; url=form.php");
		die();
}
?>