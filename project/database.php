<?php
$mysqli = new mysqli('localhost', 'galloc', 'password', 'secad_s19');
		if ($mysqli->connect_errno) {
			printf("Connect failed: %s\n", $mysqli->connect_error);
			exit();
		}
function changepassword($username, $newpassword) {
		global $mysqli;
		$prepared_sql = "UPDATE users SET password=password(?) WHERE username = ?;";
		//echo "DEBUG>prepared_sql=$prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("ss", $newpassword, $username);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
}

function addnewuser($username, $password, $firstname, $lastname, $phonenumber) {
		global $mysqli;
		$prepared_sql = "INSERT INTO users VALUES (?, password(?),?,?,?,1);";
		//echo "DEBUG:database.php->addnewuser->prepared_sql=$prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("sssss", $username, $password, $firstname, $lastname, $phonenumber);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
}

function checknewuser($username, $password) {
		global $mysqli;
		$prepared_sql = "SELECT * from users WHERE username = ? AND password=password(?);";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("ss", $username, $password);
		if(!$stmt->execute()) return FALSE;
		if(!$stmt->store_result()) echo "Store_result Error";
		$result = $stmt;
		if($result->num_rows == 1)
			return FALSE;
		return TRUE;
}

function addpost($owner, $postmessage) {
		global $mysqli;
		$prepared_sql = "INSERT INTO posts VALUES (NULL, ?, ?);";
		//echo "DEBUG:database.php->addpost->prepared_sql=$prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("ss", $owner, $postmessage);
		if(!$stmt->execute())
			 return FALSE;
		return TRUE;
}

function addcomment($commentmessage, $postid, $commentor) {
		global $mysqli;
		$prepared_sql = "INSERT INTO comments VALUES (?, ?, ?);";
		//echo "DEBUG:database.php->addcomment->prepared_sql=$prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("sss", $commentmessage, $postid, $commentor);
		if(!$stmt->execute())
			 return FALSE;
		return TRUE;
}

function editpost($postmessage, $id, $owner) {
		global $mysqli;
		$prepared_sql = "UPDATE posts SET postmessage= ? WHERE id = ? AND owner = ?;";
		//echo "DEBUG:database.php->editpost->prepared_sql=$prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("sss", $postmessage, $id, $owner);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
}

function deletepost($id) {
		global $mysqli;
		$prepared_sql = "DELETE FROM posts WHERE id = ?;";
		//echo "DEBUG:database.php->deletepost->prepared_sql=$prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("s", $id);
		if(!$stmt->execute()) echo "Execute Error";
		if(!$stmt->store_result()) echo "Store_result Error";
		$result = $stmt;
		if ($result->num_rows == 1){
			return FALSE;
		}
		return TRUE;
}

function enable($id, $username) {
		global $mysqli;
		$prepared_sql = "UPDATE users SET enabled = ? WHERE username = ?;";
		//echo "DEBUG:database.php->enable->prepared_sql=$prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("ss", $id, $username);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
}

function disable($id, $username) {
		global $mysqli;
		$prepared_sql = "UPDATE users SET enabled = ? WHERE username = ?;";
		//echo "DEBUG:database.php->disable->prepared_sql=$prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("ss", $id, $username);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
}

?>
