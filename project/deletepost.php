<?php
	require "database.php";
	require "session_auth.php";
	$id = $_POST["id"];
	$nocsrftoken = $_POST["nocrsftoken"];
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('Cross-site request forgery is detected!');</script>";
		header("Refresh:0; url=logout.php");
		die();
	}
		if(deletepost($id)){
			//echo "DEBUG:deletepost.php->Your post message $id was successfully deleted.\n";
			echo "<h4>Your post message was successfully deleted.</h4>\n";
		}else{
			echo "Your post message could not be deleted.\n";
			//echo "DEBUG: $id";
		}
?>
<a href="index.php">Feed</a> | <a href="logout.php">Logout</a>
