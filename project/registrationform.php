
<html>
      <h1>Sign Up</h1>
<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="addnewuser.php" method="POST" class="form login">
		First Name: <input type="text" class="text_field" name="firstname" required
		pattern="^[\p{L} \.'\-]+$"
                title="Please enter your first name"
                placeholder="Your first name"
                onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ");" /> <br>
		Last Name: <input type="text" class="text_field" name="lastname" required
                pattern="^[\p{L} \.'\-]+$"
                title="Please enter your last name"
                placeholder="Your last name"
                onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ");" /> <br>
		Phone Number: <input type="tel" class="text_field" name="phone" required
                pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
                title="Please enter your phone number in format 555-555-5555"
                placeholder="Your phone number"
                onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ");" /> <br>
                Username: <input type="text" class="text_field" name="username" required
                pattern="^[\w.-]+@[\w-]+(.[\w-]+)*$"
                title="Please enter a valid email as username"
                placeholder="Your email address"
                onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ");" /> <br>
                Password: <input type="password" name="password" required
                pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,}$"
                placeholder="Your password"
                title="Password must have at least 8 characters with 1 special symbol !@#$%^&, 1 number, 1 lowercase, and 1 UPPERCASE"
                onchange="this.setCustomValidity(this.validity.patternMismatch?this.title:"); form.repassword.pattern = this.value;" /> <br>
                Retype Password: <input type = "password" class = "text_field" name = "repassword"
                placeholder="Retype your password" required
                title="Password does not match"
                onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ");"/> <br>
                <button class="button" type="submit">
                  Sign Up
                </button>
          </form>
  </html>

