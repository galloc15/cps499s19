
<?php
	$lifetime = 15 * 60; //15 minutes
	$path = "/miniBook";
	$domain = "cps499s19-galloc3.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $domain, $secure, $httponly);
	session_start();
	if (isset($_POST["username"]) and isset($_POST["password"]) ){
	//if (checklogin($_POST["username"],$_POST["password"])){    
		if (securechecklogin($_POST["username"],$_POST["password"])) {
				$_SESSION["logged"] = TRUE;
				$_SESSION["username"] = $_POST["username"];
				$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
			if(superuser($_SESSION["username"],$_POST["password"])){
				$_SESSION["role"]= "superuser";
			}
			else{
				$_SESSION["role"]= "user";
			}
		}else{
			if(disablelogin($_POST["username"],$_POST["password"])){
				echo "<script>alert('Disabled username/password');</script>";
				unset($_SESSION["logged"]);
				header("Refresh:0; url=form.php");
				die();
			} else{
				echo "<script>alert('Invalid username/password');</script>";
				unset($_SESSION["logged"]);
				header("Refresh:0; url=form.php");
				die();
			}
		}
	}
	if (!isset($_SESSION["logged"] ) or $_SESSION["logged"] != TRUE) {
		echo"<script>alert('You have not login. Please login first');</script>";
		header("Refresh:0; url=form.php");
		die();
}
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
		echo "<script>alert('Session hijacking is detected!');</script>";
		header("Refresh:0; url=form.php");
		die();
}	
?>
	<h2> Welcome <?php echo htmlentities($_SESSION["username"]); ?> !</h2>
		<form action="addnewpost.php" method="POST" class="post a message">
	<input type="text" class="text_field" name="postmessage" size="50" maxlength="255"></input>
<button class="button" type="submit">
                  Post
                </button>
          </form>
	<div><?php
		displayposts();
	?>
	</div>

	<?php
	if($_SESSION["role"] == "superuser"){
		?> <a href="/miniBook/admin/listofusers.php">Enable/Disable Users</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a><?php
	}
	else{
		?> <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a><?php
	}


	function checklogin($username, $password) {
		$mysqli = new mysqli('localhost', 'galloc', 'password', 'secad_s19');
		if ($mysqli->connect_errno) {
			printf("Connect failed: %s\n", $mysqli->connect_error);
			exit();
		}
		$sql = "SELECT * FROM users WHERE username='" . $username . "'";
		$sql = $sql . " AND password=password('". $password . "');";
		echo "DEBUG> sql= $sql";
		$result = $mysqli->query($sql);
			if ($result->num_rows == 1){
			return TRUE;
		}
		return FALSE;
  	}

	function securechecklogin($username, $password) {
		$mysqli = new mysqli('localhost', 'galloc', 'password', 'secad_s19');
		if ($mysqli->connect_errno) {
			printf("Connect failed: %s\n", $mysqli->connect_error);
			exit();
		}
		$prepared_sql = "SELECT * FROM users WHERE enabled=1 AND username= ? AND password=password(?);";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username, $password);
		if(!$stmt->execute()) echo "Execute Error";
		if(!$stmt->store_result()) echo "Store_result Error";
		$result = $stmt;
		if($result->num_rows == 1)
			return TRUE;
		return FALSE;
}

	function disablelogin($username, $password) {
		$mysqli = new mysqli('localhost', 'galloc', 'password', 'secad_s19');
		if ($mysqli->connect_errno) {
			printf("Connect failed: %s\n", $mysqli->connect_error);
			exit();
		}
		$prepared_sql = "SELECT * FROM users WHERE enabled=2 AND username= ? AND password=password(?);";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username, $password);
		if(!$stmt->execute()) echo "Execute Error";
		if(!$stmt->store_result()) echo "Store_result Error";
		$result = $stmt;
		if($result->num_rows == 1)
			return TRUE;
		return FALSE;
}

	function superuser($username, $password){
		$mysqli = new mysqli('localhost', 'galloc', 'password', 'secad_s19');
		if ($mysqli->connect_errno) {
			printf("Connect failed: %s\n", $mysqli->connect_error);
			exit();
		}
		$prepared_sql = "SELECT * FROM superusers WHERE user= ? AND password=password(?);";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username, $password);
		if(!$stmt->execute()) echo "Execute Error";
		if(!$stmt->store_result()) echo "Store_result Error";
		$result = $stmt;
		if($result->num_rows == 1)
			return TRUE;
		return FALSE;
}

	function displayposts() {
		$mysqli = new mysqli('localhost', 'galloc', 'password', 'secad_s19');
		if ($mysqli->connect_errno) {
			printf("Connect failed: %s\n", $mysqli->connect_error);
			exit();
		}
		$prepared_sql = "SELECT id, owner, postmessage FROM posts;";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("sss", $id, $owner, $postmessage);
		if(!$stmt->execute()) echo "Execute Error";
		if(!$stmt->store_result()) echo "Store_result Error";
		$owner = NULL;
		$postmessage = NULL;
		$id = NULL;
		if(!$stmt->bind_result($id, $owner,$postmessage)) echo "Binding failed";
		$result = $stmt;
		while($result->fetch()){?>
			<div style="border:1px; border-style:solid; border-color:#000000; border-radius: 5px; background-color: #C0FAFF; padding: 3px; padding-bottom:0px;">
<?php
			echo "<b>" . "<font color='blue'>" . htmlentities($owner) . "</b>" . " " . "<font color='black'>" . htmlentities($postmessage) . "<br>";
			$prepared_sql = "SELECT postid, commentor, commentmessage FROM comments";
			if(!$stmt = $mysqli->prepare($prepared_sql)) echo "Prepared Statement Error";
			$stmt->bind_param("sss", $postid, $commentor, $commentmessage);
			if(!$stmt->execute()) echo "Execute Error";
			if(!$stmt->store_result()) echo "Store_result Error";
			$commentor = NULL;
			$commentmessage = NULL;
			if(!$stmt->bind_result($postid, $commentor, $commentmessage)) echo "Binding failed";
			while($stmt->fetch()){
				if ($postid == $id){
					?><div style="border:1px; border-style:solid; border-color:#000000; border-radius: 5px; background-color: #F6F2F2; padding: 3px; margin-left: 5px; margin-top:3px;"><?php echo "<b>" . "<font color='blue'>" . htmlentities($commentor) . "</b>" . " " . "<font color='black'>" . htmlentities($commentmessage) . "<br>";?></div><?php
				}
			}?>
			<form action="addnewcomment.php" method="POST" class="comment on a post">
			<input type="hidden" id="id" name="id" value="<?php echo $id?>">
			<input type="text" class="text_field" name="commentmessage" style="margin-left: 5px; margin-top:3px;" size="25" maxlength="255"></input>
<button class="button" type="submit">Comment</button></form>
<?php	
			if($owner == $_SESSION["username"]){ ?>
				<form action="editpost.php" method="POST" class="edit a post" style="display: inline-block; padding-bottom:0px;">
				<button class="button" type="submit">Edit</button>
				<?php$rand=bin2hex(openssl_random_pseudo_bytes(16));
				$_SESSION["nocsrftoken"] = $rand;?>
				<input type="hidden" name="nocrsftoken" value="<?php echo $rand; ?>"/>
				<input type="hidden" id="id" name="id" value="<?php echo $id?>"></form>
				<form action="deletepost.php" method="POST" class="delete a post" style="display: inline-block; padding-bottom:0px;">
				<?php$rand=bin2hex(openssl_random_pseudo_bytes(16));
				$_SESSION["nocsrftoken"] = $rand;?>
				<input type="hidden" name="nocrsftoken" value="<?php echo $rand; ?>"/>
				<button class="button" type="submit">Delete</button>
				<input type="hidden" id="id" name="id" value="<?php echo $id?>"></form></div><br><?php
			}else{
				?></div><br><?php
			}
		}
		
}
?>
