DROP TABLE IF EXISTS `comments`;
DROP TABLE IF EXISTS `posts`;
DROP TABLE IF EXISTS `superusers`;

CREATE TABLE `posts` (
	`id` int AUTO_INCREMENT,
	`owner` varchar(50),
	`postmessage` varchar(255),
	FOREIGN KEY (`owner`) REFERENCES `users`(`username`) ON DELETE CASCADE,
	PRIMARY KEY (`id`)
);

CREATE TABLE `comments` (
	`commentmessage` varchar(255),
	`postid` INT,
	`commentor` varchar(50),
	FOREIGN KEY (`commentor`) REFERENCES `users`(`username`) ON DELETE CASCADE,
	FOREIGN KEY (`postid`) REFERENCES `posts`(`id`) ON DELETE CASCADE
);

CREATE TABLE `superusers` (
	`user` varchar(50) NOT NULL,
	`password` varchar(100) NOT NULL,
	FOREIGN KEY (`user`) REFERENCES `users`(`username`) ON DELETE CASCADE
);
