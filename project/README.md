University of Dayton

Department of Computer Science

CPS 499 - Spring 2019

Dr. Phu Phung
# Secure Application Development Final Project #

### MiniBook ###

Any user can create an account by entering their first name, last name, phone number, username (email address), and password on the registration page. Once the account is created, the user can login to the system. From there, the user can add posts or comment on any post. The user can edit their own post, but they cannot edit someone else's post. The user can also delete their own post, but cannot delete another user's post. The user can change their password by clicking on the link at the bottom of the homepage. The user can only change their own password. Only superusers have the authority to enable or disable users. Once a user is disabled, they are not able to login until they are enabled.

The system is protected against Cross-Site Scripting (XSS) attacks, SQL Injection attacks, Cross-Site Request Forgery (CSRF) attacks, and Session Hijacking.
