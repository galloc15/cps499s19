<?php	
	require "../session_auth.php";
	$superuser = $_SESSION["username"];

	if($_SESSION["role"] == "user"){
		echo "<script>alert('You are not a superuser!');</script>";
		header("Refresh:0; url=../index.php");
		die();
	}

	listusers($superuser);

	function listusers($superuser){
		$mysqli = new mysqli('localhost', 'galloc', 'password', 'secad_s19');
		if ($mysqli->connect_errno) {
			printf("Connect failed: %s\n", $mysqli->connect_error);
			exit();
		}
		$prepared_sql = "SELECT username, enabled FROM users;";
		//echo "DEBUG> sql= $prepared_sql";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username, $enabled);
		if(!$stmt->execute()) echo "Execute Error";
		if(!$stmt->store_result()) echo "Store_result Error";
		$username = NULL;
		$enabled = NULL;
		if(!$stmt->bind_result($username, $enabled)) echo "Binding failed";
		$result = $stmt;
		while($result->fetch()){
			if($enabled == 1){
				if($username != $superuser){?>
					<form action="disable.php" method="POST" class="disable a user">
					<?php$rand=bin2hex(openssl_random_pseudo_bytes(16));
					$_SESSION["nocsrftoken"] = $rand;?>
					<input type="hidden" name="nocrsftoken" value="<?php echo $rand; ?>"/>
					<input id="checkBox" type="checkbox" name="username" value="<?php echo htmlentities($username)?>"></input><?php
					echo htmlentities($username);?>
					<button class="button" type="submit">
                  			Disable
                			</button></form><?php
				}
			} else{
				if($username != $superuser){?>
					<form action="enable.php" method="POST" class="disable a user">
					<?php$rand=bin2hex(openssl_random_pseudo_bytes(16));
					$_SESSION["nocsrftoken"] = $rand;?>
					<input type="hidden" name="nocrsftoken" value="<?php echo $rand; ?>"/>
					<input id="checkBox" type="checkbox" name="username" value="<?php echo htmlentities($username)?>"></input><?php
					echo htmlentities($username);?>
					<button class="button" type="submit">
                  			Enable
                			</button></form><?php
				}
			}?>
          		</form><?php
		}
	}
?>
		
<a href="../index.php">Home</a> | <a href="../logout.php">Logout</a>
