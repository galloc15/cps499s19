<?php
	require "../database.php";
	$username = $_POST["username"];
	$id = 1;
	$nocsrftoken = $_POST["nocrsftoken"];
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('Cross-site request forgery is detected!');</script>";
		header("Refresh:0; url=logout.php");
		die();
	}

	if($_SESSION["role"] == "user"){
		echo "<script>alert('You are not a superuser!');</script>";
		header("Refresh:0; url=../index.php");
		die();
	}

	if(enable($id, $username)){
			//echo "DEBUG:enable.php->$username was successfully enabled.\n";
			echo "<h4>$username was successfully enabled.</h4>\n";
		}else{
			echo "The user could not be enabled.\n";
			//echo "DEBUG: $username, $id";
		}
?>

<a href="listofusers.php">Enable/Disable Users</a> | <a href="../index.php">Home</a> | <a href="../logout.php">Logout</a>
